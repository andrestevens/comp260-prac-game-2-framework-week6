﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class MovePaddle : MonoBehaviour {
    private Rigidbody rigidbody;
    public float speed = 20f;
    public float force = 10f;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Vector3 pos = GetMousePosition();
        Gizmos.DrawLine(Camera.main.transform.position, pos);
    }
    private Vector3 GetMousePosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

	// Use this for initialization
	void Start ()
    {
      
    }
	
	// Update is called once per frame
	void Update ()
    {
        Debug.Log("Time = " + Time.fixedTime);
	}

    void FixedUpdate()
    {
        Vector3 pos = GetMousePosition();
        Vector3 dir = pos - rigidbody.position;
        Vector3 vel = dir.normalized * speed;

        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;

        if (move > distToTarget)
        {
            vel = vel * distToTarget / move;
        }
        rigidbody.velocity = vel;
    }
}
